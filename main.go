package main

import (
	"fmt"
	"time"

	gha "github.com/sethvargo/go-githubactions"
)

func main() {
	username := gha.GetInput("username")
	fmt.Printf("username is %s\n", username)

	gha.SetOutput("time", time.Now().Format("2006-01-02 15:04:05"))
}
